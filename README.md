# README #

### This is a repository for useful development tools ###

It will get more organized as more things are added.

*"maketheme" - Creates a basic, clean Drupal 8 theme including SASS, Susy and Breakpoints.
(This script is useful when needing to spin up a clean new theme. Could also probably be packaged with a theme as a sub theme creator utility.)